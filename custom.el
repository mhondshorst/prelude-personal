(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(column-number-mode t)
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(custom-enabled-themes (quote (leuven)))
 '(fci-rule-color "#383838")
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(package-selected-packages
   (quote
    (pug-mode request flycheck-clj-kondo sass-mode scss-mode speed-type neotree restclient-helm ag multiple-cursors company-anaconda anaconda-mode zop-to-char zenburn-theme yaml-mode yafolding which-key volatile-highlights undo-tree super-save smartrep smartparens rainbow-mode rainbow-delimiters operate-on-number move-text magit-popup magit lsp-ui json-mode js2-mode imenu-anywhere hl-todo helm-projectile guru-mode gitignore-mode gitconfig-mode git-timemachine gist geiser flycheck expand-region exec-path-from-shell elpy elisp-slime-nav editorconfig easy-kill dockerfile-mode discover-my-major diminish diff-hl csv-mode crux counsel company-lsp cider browse-kill-ring beacon anzu ace-window aa-edit-mode)))
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(pug-tab-width 4)
 '(safe-local-variable-values
   (quote
    ((cider-default-cljs-repl . shadow)
     (cider-shadow-cljs-default-options . "app")
     (cider-ns-refresh-after-fn . "integrant.repl/resume")
     (cider-ns-refresh-before-fn . "integrant.repl/suspend"))))
 '(size-indication-mode t)
 '(sql-connection-alist
   (quote
    (("zorgrank_35432"
      (sql-product
       (quote postgres))
      (sql-user "mq_zorgrank_user")
      (sql-server "localhost")
      (sql-database "mq_zorgrank")
      (sql-port 35432))
     ("zorgrank_cronjob_5432"
      (sql-product
       (quote postgres))
      (sql-user "mq_zorgrank_user")
      (sql-password "")
      (sql-server "localhost")
      (sql-database "mq_zorgrank")
      (sql-port 5432))
     ("continu_meten_dev_25432"
      (sql-product
       (quote postgres))
      (sql-user "continu_meten_user")
      (sql-password "continu_meten")
      (sql-server "localhost")
      (sql-database "continu_meten")
      (sql-port 25432))
     ("zorgrank-cloudsql-test"
      (sql-product
       (quote postgres))
      (sql-user "mq_zorgrank_user")
      (sql-password "mq_zorgrank_password")
      (sql-server "127.0.0.1")
      (sql-database "mq_zorgrank")
      (sql-port 54325))
     ("continu_meten_cloudsql_54325"
      (sql-product
       (quote postgres))
      (sql-user "continu_meten_user")
      (sql-server "localhost")
      (sql-database "continu_meten")
      (sql-port 54325)))))
 '(sql-database "mq_zorgrank_test")
 '(sql-electric-stuff (quote semicolon))
 '(sql-port 5432)
 '(tool-bar-mode nil)
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-color-map
   (quote
    ((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Ubuntu Mono" :foundry "DAMA" :slant normal :weight normal :height 105 :width normal)))))

(setq-default fill-column 80)
