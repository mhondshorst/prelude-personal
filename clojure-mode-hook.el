(add-hook 'clojure-mode-hook
          (lambda () (add-hook 'before-save-hook 'cider-format-buffer nil 'local)))
